var button = document.getElementById("but");
var eight = document.getElementById("eigh");
var answer = document.getElementById("aner");
var input = document.getElementById("in");
var options = [
  "It is certain",
  "It is decidedly so",
  "Without a doubt",
  "Yes , definitely",
  "You may rely on it",
  "It only in the stars",
  "Maybe",
  "Outlook good",
  "Yes",
  "Signs point to yes",
  "Girl bye",
  "Hell No",
  "My sources say no",
  "Whatever",
  "Very doubtful",
  "Reply hazy, try again",
  "Try, Try, Again ",
  "Better not tell you now",
  "Maybe next Life time",
  "Concentrate and ask again"
];

button.addEventListener("click", function() {
  if (input.value.length < 10) {
    alert(" Please enter a fun question!");
  } else {
    eight.innerText = "";
    let num = input.value.length % options.length;
    answer.innerText = options[num];
  }
});
